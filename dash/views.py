from django.shortcuts import render
from usuarios.models import Usuarios
from ligacoes.models import Ligacoes
from propostas.models import Adiantamentos, Propostas
from datetime import datetime
from listas import Listas
from django.db.models import F, Sum
from utils import Utils

@Utils.Authenticate
def DashView(request):
    usuario = Usuarios.objects.get(id=request.session['user_key'])
    comissoes = Propostas.objects.values('corretor__nome')\
                                 .filter(pagamento=True, cliente=False, data_pagamento__month=datetime.now().month, data_pagamento__year=datetime.now().year)\
                                 .annotate(total=Sum((F('taxaLucro') / 100) * F('vlBruto')), nome=F('corretor__nome') )

    mensal = Propostas.objects.filter(pagamento=True, cliente=False, data_pagamento__month=datetime.now().month, data_pagamento__year=datetime.now().year, taxaLucro__gte=0).annotate(total=Sum((F('taxaLucro') / 100) * F('vlBruto')))
    anual = Propostas.objects.filter(pagamento=True, cliente=False, data_pagamento__year=datetime.now().year, taxaLucro__gte=0).annotate(total=Sum((F('taxaLucro') / 100) * F('vlBruto')))
    pendentes = Propostas.objects.filter(dataCadastro__year=datetime.now().year, pagamento=False).count()
    producaoMes = Propostas.objects.values('data_pagamento__month').filter(pagamento=True, cliente=False, data_pagamento__year=datetime.now().year).annotate(total=Sum((F('taxa') / 100) * F('vlBruto')))
    totalMes = []
    pagamentosPendentes = Propostas.objects.filter(pagamento=False, status=0, cliente=False)

    for i in range(0, producaoMes[0]['data_pagamento__month']):
        totalMes.append('0')

    for p in producaoMes:
        totalMes.append(p['total'])


    producao = {
        'mensal': sum(m.total for m in mensal),
        'anual': sum(a.total for a in anual),
        'pendentes': pendentes,
        'meses': [Listas.meses[i] for i in range(0, datetime.now().month)],
        'producoes': totalMes,
        'pagamentosPendentes': pagamentosPendentes,
        'listaPagamento': list(Listas.tiposDeConta)
    }



    data = {
        'comissoes': comissoes,
        'usuario': usuario,
        'producao': producao,
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day,
                                                data_agendamento__month=datetime.now().month,
                                                data_agendamento__year=datetime.now().year).count(),
    }

    return render(request, 'dash/dash.html', data)
