$(function() {

    if(isMobile()){
        $('#sidebarToggleTop').click();
    }

    $("tr").click(function(e) {
        if($(this).get(0).id > 0)
        {   
            $('#diasUsoSection').fadeOut('slow');

            $('input[name=id]').val($(this).get(0).id);
            $('input[name=nome]').val($(this).find('td:nth-child(1)').html());

            $('#mesUso').length > 0 ? $('#mesUso').val(new Date().getMonth() + 1) : null;

            ifExists('email') ? $('input[name=email]').val($(this).find('td:nth-child(2)').html()) : null;
            ifExists('endereco') ? $('input[name=endereco]').val($(this).find('td:nth-child(2)').html()) : null;
            ifExists('descricao') ? $('input[name=descricao]').val($(this).find('td:nth-child(2)').html()) : null;

            ifExists('valor') ? $('input[name=valor]').val($(this).find('td:nth-child(3)').html().split(' ')[1]) : null;
            ifExists('telefone') ? $('input[name=telefone]').val($(this).find('td:nth-child(3)').html()) : null;

            ifExists('quantidade') ? $('input[name=quantidade]').val($(this).find('td:nth-child(4)').html().split(' ')[0]) : null;

            selectOption('grupo', '3', $(this));
            selectOption('status', '5', $(this));
            selectOption('categoria', '5', $(this));
            selectOption('fornecedor', '6', $(this));

            $("#edicaoUsuarioModal").fadeIn('slow');
        }
        
    });

    $('#FormButtonContainer a:nth-child(1)').click(function(){
        $("#edicaoUsuarioModal").fadeOut('slow');
    });

});

function selectOption(nome, opcao, elemento)
{
    var nomeObjeto = 'select[name=' + nome + ']';
    var objeto = $(nomeObjeto).children('option');
    var item = 'td:nth-child(' + opcao + ')';
    
    for(var i=0; i < $(nomeObjeto).find('option').length; i++)
    {
        //console.log(elemento.attr('data-' + nome));
        if(objeto.get(i).innerHTML == elemento.find(item).html() || objeto.get(i).innerHTML == elemento.attr('data-' + nome))
        {
            $(nomeObjeto).val(objeto.get(i).value);
        }
    }
}

function ifExists(name) {
    return $('input[name=' + name + ']').length;
}

function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
}