from django.shortcuts import render
from usuarios.models import Usuarios
from propostas.models import Propostas
from propostas.views import FiltroCPF
from .models import Ligacoes
from utils import Utils
from datetime import datetime


@Utils.Authenticate
def LigacoesView(request):
    operation = request.GET.get('operation', 'add')
    usuario = Usuarios.objects.get(id=request.session['user_key'])
    edicao = FiltroCPF(request.GET.get('c', None), request.GET.get('id', None))
    ligacao = None

    if edicao is not None:
        edicao = Propostas.objects.get(id=edicao)
        if Ligacoes.objects.filter(cliente__id=edicao.id).count() > 0:
            ligacao = Ligacoes.objects.filter(cliente__id=edicao.id)

    if request.method == 'POST':
        Ligacoes(cliente=edicao,
                 data_agendamento=request.POST['agendamento'],
                 usuario=usuario,
                 observacao=request.POST['observacao']).save()

    data = {
        'operation': operation,
        'usuario': usuario,
        'propostas': Propostas.objects.filter(cliente=True),
        'edicao': edicao,
        'ligacao': ligacao,
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count()
    }

    return render(request, 'ligacoes/ligacoes.html', data)
