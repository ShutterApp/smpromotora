# Generated by Django 2.2.1 on 2019-10-21 11:39

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ligacoes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ligacoes',
            name='data_ligacao',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 10, 21, 11, 39, 32, 485009), null=True),
        ),
    ]
