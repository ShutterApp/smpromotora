# Generated by Django 2.2.1 on 2019-10-22 12:34

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ligacoes', '0002_auto_20191021_1139'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ligacoes',
            name='data_ligacao',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 10, 22, 12, 34, 52, 599419), null=True),
        ),
    ]
