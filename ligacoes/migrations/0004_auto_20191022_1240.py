# Generated by Django 2.2.1 on 2019-10-22 12:40

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ligacoes', '0003_auto_20191022_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ligacoes',
            name='data_ligacao',
            field=models.DateField(blank=True, default=datetime.datetime(2019, 10, 22, 12, 40, 15, 517633), null=True),
        ),
    ]
