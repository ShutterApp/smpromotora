from django.apps import AppConfig


class LigacoesConfig(AppConfig):
    name = 'ligacoes'
