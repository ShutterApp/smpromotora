from django.db import models
from propostas.models import Propostas
from datetime import datetime
from usuarios.models import Usuarios

class Ligacoes(models.Model):

    cliente = models.ForeignKey(Propostas, on_delete=models.CASCADE, blank=True, null=True)
    data_ligacao = models.DateField(default=datetime.now(), blank=True, null=True)
    data_agendamento = models.DateField(blank=True, null=True)
    observacao = models.CharField(blank=True, null=True, max_length=254)
    usuario = models.ForeignKey(Usuarios, on_delete=models.CASCADE, blank=True, null=True)