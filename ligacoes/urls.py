from django.urls import path

from . import views

urlpatterns = [
    path('ligacoes/', views.LigacoesView, name='ligacoes'),
]