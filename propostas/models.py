from django.db import models
from multiselectfield import MultiSelectField
from listas import Listas
from datetime import datetime
from usuarios.models import Usuarios




class Adiantamentos(models.Model):

    corretor = models.ForeignKey(Usuarios, on_delete=models.CASCADE, blank=True, null=True)
    valor = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    observacao = models.CharField(max_length=254, blank=True, null=True)
    data_retirada = models.DateTimeField(default=datetime.now(), blank=True, null=True)




class Propostas(models.Model):

    contrato = models.CharField(max_length=254, blank=True, null=True)
    banco = models.CharField(max_length=254, blank=True, null=True)
    produto = MultiSelectField(choices=list(Listas.listaProdutos), blank=True, null=True)

    nome = models.CharField(max_length=254, blank=True, null=True)
    sexo = models.CharField(max_length=254, blank=True, null=True)
    dataNascimento = models.DateTimeField(blank=True, null=True)
    cpf = models.CharField(max_length=254, blank=True, null=True)
    rg = models.CharField(max_length=254, blank=True, null=True)
    emissao = models.DateTimeField(blank=True, null=True)
    naturalidade = models.CharField(max_length=254, blank=True, null=True)
    uf = models.CharField(max_length=5, blank=True, null=True)
    nomeMae = models.CharField(max_length=254, blank=True, null=True)
    nomePai = models.CharField(max_length=254, blank=True, null=True)
    endereco = models.CharField(max_length=254, blank=True, null=True)
    numero = models.IntegerField(default=0, blank=True, null=True)
    bairro = models.CharField(max_length=254, blank=True, null=True)
    cidade = models.CharField(max_length=254, blank=True, null=True)
    estado = models.CharField(max_length=254, blank=True, null=True)
    cep = models.CharField(max_length=20, blank=True, null=True)
    orgao = MultiSelectField(choices=list(Listas.listaOrgaos))
    funcao = models.CharField(max_length=254, blank=True, null=True)
    senha = models.CharField(max_length=254, blank=True, null=True)
    matricula = models.CharField(max_length=254, blank=True, null=True)
    vlParcela = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    renda = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dataAdmissao = models.DateTimeField(blank=True, null=True)
    celular = models.CharField(max_length=254, blank=True, null=True)
    fixo = models.CharField(max_length=254, blank=True, null=True)
    conta = models.CharField(max_length=20, blank=True, null=True)
    agencia = models.CharField(max_length=15, blank=True, null=True)
    tipo = models.CharField(max_length=15, blank=True, null=True)

    fidentidade = models.FileField(blank=True, null=True)
    comprovanteR = models.FileField(blank=True, null=True)
    extrato = models.FileField(blank=True, null=True)
    complemento = models.FileField(blank=True, null=True)

    corretor = models.ForeignKey(Usuarios, on_delete=models.CASCADE, blank=True, null=True)
    observacao = models.CharField(max_length=254, blank=True, null=True)
    vlBruto = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    vlLiquido = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    taxa = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    taxaJuros = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    nb = models.CharField(max_length=20, blank=True, null=True)
    prazo = models.IntegerField(default=0, blank=True, null=True)
    status = models.IntegerField(default=2, blank=True, null=True)
    tipoDeposito = models.CharField(max_length=15, blank=True, null=True)
    bancoDeposito = models.CharField(max_length=254, blank=True, null=True)
    dataCadastro = models.DateTimeField(default=datetime.now(), blank=True, null=True)
    correspondente = models.CharField(max_length=254, blank=True, null=True)
    cliente = models.BooleanField(default=False, blank=True, null=True)
    pagamento = models.BooleanField(default=False, blank=True, null=True)
    data_pagamento = models.DateTimeField(blank=True, null=True)
    taxaLucro = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)

    data_liberacao = models.DateTimeField(blank=True, null=True)

    @property
    def comissao(self):
        return (float(self.taxa) / 100) * float(self.vlBruto)


