from django.urls import path

from . import views

urlpatterns = [
    path('propostas/', views.PropostasView, name='propostas'),
    path('propostas/baixas', views.BaixasView, name='baixa'),
    path('propostas/clientes', views.ClientesView, name='clientes'),
    path('propostas/adiantamentos', views.AdiantamentoView, name='adiantamentos'),
]