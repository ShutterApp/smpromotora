from django.contrib import admin
from propostas.models import Propostas, Adiantamentos

admin.site.register(Propostas)
admin.site.register(Adiantamentos)