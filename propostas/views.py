from itertools import product

from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect
from usuarios.models import Usuarios
from propostas.models import Propostas
from .models import Adiantamentos
from listas import Listas
from utils import Utils
from ligacoes.models import Ligacoes
from datetime import datetime


@Utils.Authenticate
def PropostasView(request):

    operation = request.GET.get('operation', 'add')
    usuario = Usuarios.objects.get(id=request.session['user_key'])
    edicao = FiltroCPF(request.GET.get('c', None), request.GET.get('id', None))
    corretor = request.POST.get('corretor', 0)
    propostas = Propostas.objects.filter(cliente=False, corretor=usuario).order_by('-dataCadastro') if usuario.nivel == 'Corretor' else Propostas.objects.filter(cliente=False).order_by('-dataCadastro')


    if edicao is not None:
        edicao = Propostas.objects.get(id=edicao)

    if(request.method == 'POST'):

        request.POST = request.POST.copy()
        request.POST = ValidateFields(request.POST)

        if edicao is not None and not bool(request.GET.get('template', 0)):

            edicao.contrato=request.POST['contrato']
            edicao.banco = request.POST['banco']
            edicao.produto = request.POST['produto']
            edicao.nome = request.POST['nome']
            edicao.sexo = request.POST['sexo']
            edicao.cpf = request.POST['cpf']
            edicao.rg = request.POST['rg']
            edicao.emissao = request.POST['dataEmissao']
            edicao.dataNascimento = request.POST['dataNascimento']
            edicao.naturalidade = request.POST['naturalidade']
            edicao.uf = request.POST['uf']
            edicao.nomePai = request.POST['nomePai']
            edicao.nomeMae = request.POST['nomeMae']
            edicao.endereco = request.POST['endereco']
            edicao.numero = request.POST['numero']
            edicao.bairro = request.POST['bairro']
            edicao.cidade = request.POST['cidade']
            edicao.estado = request.POST['estado']
            edicao.cep = request.POST['cep']
            edicao.orgao = request.POST['orgao']
            edicao.funcao = request.POST['funcao']
            edicao.matricula = request.POST['matricula']
            edicao.senha = request.POST['senha']
            edicao.vlParcela = request.POST['valorParcela'].replace(',', '.')
            edicao.prazo = request.POST['prazo']
            edicao.vlBruto = request.POST['valorBruto'].replace(',', '.')
            edicao.vlLiquido = request.POST['valorLiquido'].replace(',', '.')
            edicao.renda = request.POST['renda'].replace(',', '.')
            edicao.dataAdmissao = request.POST['dataAdmissao']
            edicao.celular = request.POST['numeroCelular']
            edicao.fixo = request.POST['numeroFixo']
            edicao.correspondente = request.POST['correspondente']
            edicao.bancoDeposito = request.POST['bancoDeposito']
            edicao.agencia = request.POST['agenciaPagamento']
            edicao.data_liberacao = request.POST['data_liberacao']
            edicao.conta = request.POST['contaPagamento']
            edicao.tipoDeposito = request.POST['tipoPagamento']
            edicao.observacao = request.POST['observacao']
            edicao.taxaJuros = request.POST['taxaJuros'].replace(',', '.')
            edicao.nb = request.POST['nb'].replace(',', '.')
            edicao.corretor = Usuarios.objects.get(id=request.POST['corretor'])

            edicao.save()

            if request.FILES.get('fidentidade', None) is not None:
                edicao.fidentidade = UploadArquivo(request.FILES['fidentidade'], 'Identidade', corretor, edicao)
            if request.FILES.get('comprovanteR', None) is not None:
                edicao.comprovanteR = UploadArquivo(request.FILES['comprovanteR'], 'Comprovante', corretor, edicao)
            if request.FILES.get('extrato', None) is not None:
                edicao.extrato = UploadArquivo(request.FILES['extrato'], 'Extrato', corretor, edicao)
            if request.FILES.get('complemento', None) is not None:
                edicao.complemento = UploadArquivo(request.FILES['complemento'], 'Complemento', corretor, edicao)

            edicao.save()
            return redirect('../propostas/?operation=list')
        else:

            proposta = Propostas(contrato=request.POST['contrato'],
                      banco=request.POST['banco'],
                      produto=request.POST['produto'],
                      nome=request.POST['nome'],
                      sexo=request.POST['sexo'],
                      cpf=request.POST['cpf'],
                      rg=request.POST['rg'],
                      emissao=request.POST['dataEmissao'],
                      dataNascimento=request.POST['dataNascimento'],
                      naturalidade=request.POST['naturalidade'],
                      uf=request.POST['uf'],
                      nomePai=request.POST['nomePai'],
                      nomeMae=request.POST['nomeMae'],
                      endereco=request.POST['endereco'],
                      numero=request.POST['numero'] if request.POST['numero'] != '' else None,
                      bairro=request.POST['bairro'],
                      cidade=request.POST['cidade'],
                      estado=request.POST['estado'],
                      cep=request.POST['cep'],
                      orgao=request.POST['orgao'],
                      funcao=request.POST['funcao'],
                      matricula=request.POST['matricula'],
                      senha=request.POST['senha'],
                      vlParcela=request.POST['valorParcela'].replace(',', '.'),
                      prazo=request.POST['prazo'] if request.POST['prazo'] != '' else None,
                      vlBruto=request.POST['valorBruto'].replace(',', '.'),
                      vlLiquido=request.POST['valorLiquido'].replace(',', '.'),
                      renda=request.POST['renda'].replace(',', '.'),
                      dataAdmissao=request.POST['dataAdmissao'],
                      celular=request.POST['numeroCelular'],
                      fixo=request.POST['numeroFixo'],
                      correspondente=request.POST['correspondente'],
                      bancoDeposito=request.POST['bancoDeposito'],
                      agencia=request.POST['agenciaPagamento'],
                      conta=request.POST['contaPagamento'],
                      data_liberacao=request.POST['data_liberacao'],
                      tipoDeposito=request.POST['tipoPagamento'],
                      observacao=request.POST['observacao'],
                      taxaJuros = request.POST['taxaJuros'].replace(',', '.'),
                      nb = request.POST['nb'].replace(',', '.'),
                      corretor=Usuarios.objects.get(id=request.POST['corretor']),
                      cliente=False)
            proposta.save()

            if request.FILES.get('fidentidade', None) is not None:
                proposta.fidentidade = UploadArquivo(request.FILES['fidentidade'], 'Identidade', corretor, proposta)
            if request.FILES.get('comprovanteR', None) is not None:
                proposta.comprovanteR = UploadArquivo(request.FILES['comprovanteR'], 'Comprovante', corretor, proposta)
            if request.FILES.get('extrato', None) is not None:
                proposta.extrato = UploadArquivo(request.FILES['extrato'], 'Extrato', corretor, proposta)
            if request.FILES.get('complemento', None) is not None:
                proposta.complemento = UploadArquivo(request.FILES['complemento'], 'Complemento', corretor, proposta)

            proposta.save()
            return redirect('../propostas/?operation=list')

    data = {
        'operation': operation,
        'usuario': usuario,
        'edicao': edicao,
        'tiposDeConta': Listas.tiposDeConta,
        'bancos': Listas.bancos,
        'propostas': propostas,
        'listaProdutos': Listas.listaProdutos,
        'listaOrgaos': Listas.listaOrgaos,
        'listaStatus': Listas.statusProposta,
        'corretores': Usuarios.objects.filter(status=True),
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count()
    }

    return render(request, 'propostas/adicionar.html', data)




def UploadArquivo(arquivo, nome, corretor, proposta):
    path = ''
    if arquivo.name is not None:
        myfile = arquivo
        path = 'documentos/' + str(corretor.id) + '/' + str(proposta.id) + '/' + nome + '.' + myfile.name.split('.')[1]
        fs = FileSystemStorage()
        if fs.exists(path):
            fs.delete(path)
        filename = fs.save(path, myfile)

    return path




def ValidateFields(values):
    result = values
    numberFields = ['id', 'contrato', 'numero', 'valorParcela', 'prazo', 'valorBruto', 'valorLiquido', 'renda', 'taxa', 'taxaJuros', 'taxaLucro']
    dateFields = ['dataAdmissao', 'dataEmissao', 'dataNascimento']

    for nF in numberFields:
        if result.get(nF, None) is not None:
            if result[nF] is '':
                result[nF] = '0'

    for dF in dateFields:
        if result.get(dF, None) is not None:
            if result[dF] is '':
                result[dF] = None

    return result




@Utils.Authenticate
def BaixasView(request):
    usuario = Usuarios.objects.get(id=request.session['user_key'])
    edicao = Propostas.objects.get(id=int(request.GET['id']))

    request.POST = request.POST.copy()
    request.POST = ValidateFields(request.POST)

    if request.method == 'POST':
        edicao.taxa = request.POST['taxa'].replace(',', '.')
        edicao.taxaLucro = request.POST['taxaLucro'].replace(',', '.')
        edicao.vlBruto = request.POST['valorBruto'].replace(',', '.')
        edicao.vlLiquido = request.POST['valorLiquido'].replace(',', '.')
        edicao.vlParcela = request.POST['valorParcela'].replace(',', '.')
        edicao.prazo = request.POST['prazo']
        edicao.status = request.POST['status']
        edicao.corretor = Usuarios.objects.get(id=int(request.POST['corretor']))
        edicao.pagamento = request.POST['pagamento'] is '1'
        if request.POST['pagamento'] is '1':
            edicao.data_pagamento = datetime.now()
            edicao.status = 0
        edicao.save()
        return redirect('../propostas/?operation=list')

    data = {
        'status': Listas.statusProposta,
        'edicao': edicao,
        'usuario': usuario,
        'corretor': Usuarios.objects.all(),
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count()
    }

    return render(request, 'propostas/baixa.html', data)




def FiltroCPF(cpf, edicao):
    pesquisa = cpf
    resultado = None

    if pesquisa is not None:
        proposta = Propostas.objects.filter(cliente=True, cpf=pesquisa)
        if proposta.count() > 0:
            resultado = proposta[0].id
    else:
        resultado = edicao

    return resultado




@Utils.Authenticate
def ClientesView(request):

    usuario = Usuarios.objects.get(id=request.session['user_key'])
    operation = request.GET.get('operation', 'add')
    edicao = FiltroCPF(request.GET.get('c', None), request.GET.get('id', None))

    if edicao is not None:
        edicao = Propostas.objects.get(id=edicao)

    if (request.method == 'POST'):

        request.POST = request.POST.copy()
        request.POST = ValidateFields(request.POST)

        if edicao is not None:
            edicao.nome = request.POST['nome']
            edicao.sexo = request.POST['sexo']
            edicao.cpf = request.POST['cpf']
            edicao.rg = request.POST['rg']
            edicao.emissao = request.POST['dataEmissao']
            edicao.dataNascimento = request.POST['dataNascimento']
            edicao.naturalidade = request.POST['naturalidade']
            edicao.uf = request.POST['uf']
            edicao.nomePai = request.POST['nomePai']
            edicao.nomeMae = request.POST['nomeMae']
            edicao.endereco = request.POST['endereco']
            edicao.numero = request.POST['numero']
            edicao.bairro = request.POST['bairro']
            edicao.cidade = request.POST['cidade']
            edicao.estado = request.POST['estado']
            edicao.cep = request.POST['cep']
            edicao.orgao = request.POST['orgao']
            edicao.funcao = request.POST['funcao']
            edicao.matricula = request.POST['matricula']
            edicao.nb = request.POST['nb'] + ';' + request.POST['nb1'] + ';' + request.POST['nb2'] + ';' + request.POST['nb3']
            edicao.senha = request.POST['senha']
            edicao.renda = request.POST['renda'].replace(',', '.')
            edicao.celular = request.POST['numeroCelular']
            edicao.fixo = request.POST['numeroFixo']
            edicao.bancoDeposito = request.POST['bancoDeposito']
            edicao.agencia = request.POST['agenciaPagamento']
            edicao.conta = request.POST['contaPagamento']
            edicao.tipoDeposito = request.POST['tipoPagamento']
            edicao.observacao = request.POST['observacao']
            edicao.save()
            return redirect('../propostas/clientes?operation=list')
        else:
            proposta = Propostas(nome=request.POST['nome'],
                                 sexo=request.POST['sexo'],
                                 cpf=request.POST['cpf'],
                                 rg=request.POST['rg'],
                                 emissao=request.POST['dataEmissao'],
                                 dataNascimento=request.POST['dataNascimento'],
                                 naturalidade=request.POST['naturalidade'],
                                 uf=request.POST['uf'],
                                 nomePai=request.POST['nomePai'],
                                 nomeMae=request.POST['nomeMae'],
                                 endereco=request.POST['endereco'],
                                 numero=request.POST['numero'],
                                 bairro=request.POST['bairro'],
                                 cidade=request.POST['cidade'],
                                 estado=request.POST['estado'],
                                 cep=request.POST['cep'],
                                 orgao=request.POST['orgao'],
                                 funcao=request.POST['funcao'],
                                 matricula=request.POST['matricula'],
                                 nb=request.POST['nb'] + ';' + request.POST['nb1'] + ';' + request.POST['nb2'] + ';' + request.POST['nb3'],
                                 senha=request.POST['senha'],
                                 renda=request.POST['renda'].replace(',', '.'),
                                 celular=request.POST['numeroCelular'],
                                 fixo=request.POST['numeroFixo'],
                                 bancoDeposito=request.POST['bancoDeposito'],
                                 agencia=request.POST['agenciaPagamento'],
                                 conta=request.POST['contaPagamento'],
                                 tipoDeposito=request.POST['tipoPagamento'],
                                 observacao=request.POST['observacao'],
                                 cliente=True)
            proposta.save()
            return redirect('../propostas/clientes?operation=list')

    data = {
        'operation': operation,
        'usuario': usuario,
        'edicao': edicao,
        'tiposDeConta': Listas.tiposDeConta,
        'bancos': Listas.bancos,
        'listaProdutos': Listas.listaProdutos,
        'listaOrgaos': Listas.listaOrgaos,
        'propostas': Propostas.objects.filter(cliente=True),
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count()
    }

    return render(request, 'propostas/clientes.html', data)




@Utils.Authenticate
def AdiantamentoView(request):
    usuario = Usuarios.objects.get(id=request.session['user_key'])
    corretor = Usuarios.objects.get(id=request.GET.get('id', None))

    if request.method == 'POST':
        Adiantamentos(
            valor=request.POST['renda'].replace(',', '.'),
            observacao=request.POST['observacao'],
            corretor=corretor
        ).save()

    data = {
        'usuario': usuario,
        'corretor': Usuarios.objects.get(id=corretor.id),
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count(),
        'adiantamentos': Adiantamentos.objects.filter(corretor=corretor)
    }

    return render(request, 'propostas/adiantamento.html', data)