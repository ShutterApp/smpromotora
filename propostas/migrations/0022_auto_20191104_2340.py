# Generated by Django 2.2.1 on 2019-11-04 23:40

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('propostas', '0021_auto_20191031_1235'),
    ]

    operations = [
        migrations.AddField(
            model_name='propostas',
            name='taxaImposto',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True),
        ),
        migrations.AlterField(
            model_name='adiantamentos',
            name='data_retirada',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 11, 4, 23, 40, 9, 622292), null=True),
        ),
        migrations.AlterField(
            model_name='propostas',
            name='dataCadastro',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 11, 4, 23, 40, 9, 625418), null=True),
        ),
    ]
