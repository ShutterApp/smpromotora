# Generated by Django 2.2.1 on 2019-11-16 20:05

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('propostas', '0024_auto_20191106_0026'),
    ]

    operations = [
        migrations.AddField(
            model_name='propostas',
            name='taxaLucro',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True),
        ),
        migrations.AlterField(
            model_name='adiantamentos',
            name='data_retirada',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 11, 16, 20, 5, 29, 578403), null=True),
        ),
        migrations.AlterField(
            model_name='propostas',
            name='dataCadastro',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2019, 11, 16, 20, 5, 29, 581507), null=True),
        ),
    ]
