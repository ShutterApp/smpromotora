from django.shortcuts import redirect


def Authenticate(func):

    def decorator(request, *args, **kwargs):
        auth = request.session.get('user_key', None)

        if func.__name__ is not 'LoginView':
            if auth is not None:
                return func(request, *args, **kwargs)
            else:
                return redirect('/')

        else:
            return func(request, *args, **kwargs)

    return decorator