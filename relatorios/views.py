from django.shortcuts import render
from listas import Listas
from propostas.models import Propostas, Adiantamentos
from usuarios.models import Usuarios
from django.db.models import F, Sum
from django.core import serializers
from datetime import datetime
from usuarios.models import Usuarios

def RelatoriosView(request):

    proposta = Propostas.objects.filter(cliente=False).order_by('-id')
    usuario = Usuarios.objects.get(id=request.session['user_key'])

    if request.method == 'POST':
        proposta = Propostas.objects.filter(cliente=False,
                                            data_pagamento__range=[request.POST['inicio'], request.POST['fim']],
                                            # dataCadastro__range=[request.POST['inicio'], request.POST['fim']],
                                            corretor__id=request.POST['corretor'],
                                            status=request.POST['status']).annotate(total=(F('taxa') / 100) * F('vlBruto')).order_by('-id')
        request.session['filtro'] = request.POST

    data = {
        'corretores': Usuarios.objects.all(),
        'status': Listas.statusProposta,
        'propostas': proposta,
        'usuario': usuario,
    }

    return render(request, 'relatorios/relatorios.html', data)



def ResumoView(request):

    usuario = Usuarios.objects.get(id=request.session['user_key'])
    filtro = request.session['filtro']

    proposta = Propostas.objects.filter(cliente=False,
                                        dataCadastro__range=[filtro['inicio'], filtro['fim']],
                                        corretor__id=filtro['corretor'],
                                        status=filtro['status']).annotate(total=(F('taxa') / 100) * F('vlBruto')).order_by('-id')

    data = {
        'usuario': usuario,
        'proposta': proposta,
        'corretor': Usuarios.objects.get(id=filtro['corretor']),
        'adiantamentos': Adiantamentos.objects.filter(data_retirada__month=datetime.strptime(filtro['fim'], '%Y-%m-%d').month, corretor__id=filtro['corretor'])
    }

    return render(request, 'relatorios/resumo.html', data)



def ResumoPropostaView(request):

    edicao = request.GET.get('id', None)
    # usuario = Usuarios.objects.get(id=request.session['user_key'])

    if edicao is not None:
        edicao = Propostas.objects.get(id=edicao)

    data = {
        # 'usuario': usuario,
        'edicao': edicao,
        'tiposDeConta': Listas.tiposDeConta,
        'bancos': Listas.bancos,
        'listaProdutos': Listas.listaProdutos,
        'listaOrgaos': Listas.listaOrgaos,
        'listaStatus': Listas.statusProposta,
        'corretores': Usuarios.objects.all()
    }

    return render(request, 'relatorios/proposta.html', data)