from django.urls import path

from . import views

urlpatterns = [
    path('relatorios/', views.RelatoriosView, name='relatorios'),
    path('relatorios/resumo', views.ResumoView, name='resumo'),
    path('relatorios/proposta', views.ResumoPropostaView, name='resumoproposta'),
]