from django import template
from decimal import Decimal

register = template.Library()

# template tag com argumento
# @register.filter
# def remainder(value, arg):
#     return value % arg

@register.filter
def somaTotais(value):
    soma = sum(v.total for v in value)
    return soma

@register.filter
def somaPagamentos(value):
    soma = sum(v.valor for v in value)
    return soma

@register.filter
def getNB(value, args):
    x = str(value).split(';')
    if len( x ) == 1:
        x = ';;;;'
    value = x[args]
    return value

@register.filter
def NBs(value):
    x = str(value).split(';')
    if len( x ) == 1:
        x = ';;;;'
    value = x
    return value


@register.filter
def subtrair(value, args):
    return sum(v.total for v in value) - sum(v.valor for v in args)