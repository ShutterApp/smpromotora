from django import forms

from .models import Usuarios

class FormUsuarios(forms.ModelForm):
    
    class Meta:
        model = Usuarios
        fields = ('email', 'senha')
        widgets = {
            'email': forms.TextInput(attrs={
                'placeholder': 'Email',
                'class': 'form-control form-control-user'
            }),
            'senha': forms.PasswordInput(attrs={
                'placeholder': 'Senha',
                'class': 'form-control form-control-user'
            })
        }
