# Generated by Django 2.2.1 on 2019-11-04 23:40

from django.db import migrations
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('usuarios', '0004_auto_20191022_1234'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuarios',
            name='banco',
            field=multiselectfield.db.fields.MultiSelectField(choices=[[0, 'Banco do Brasil '], [1, 'Banrisul'], [2, 'BGN'], [3, 'BMG'], [4, 'Bonsucesso'], [5, 'Bradesco'], [6, 'Bradesco'], [7, 'BV'], [8, 'Caixa'], [9, 'CCB'], [10, 'Cetelen'], [11, 'Crefisa'], [12, 'Daycoval'], [13, 'Help'], [14, 'Industrial'], [15, 'Intermedium'], [16, 'Itaú BMG'], [17, 'Mercantil'], [18, 'Olé'], [19, 'PAN'], [20, 'Paraná'], [21, 'Sabemi'], [22, 'Safra'], [23, 'Santander']], max_length=61),
        ),
        migrations.AlterField(
            model_name='usuarios',
            name='tipo',
            field=multiselectfield.db.fields.MultiSelectField(choices=[[0, 'Corrente Pessoa Física'], [1, 'Poupança Pessoa Física'], [2, 'Corrente Pessoa Jurídica'], [1, 'Poupança Pessoa Jurídica']], max_length=7),
        ),
    ]
