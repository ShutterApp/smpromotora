from django.urls import path

from . import views

urlpatterns = [
    path('', views.LoginView, name='login'),
    path('usuarios/', views.UsuariosView, name='usuarios'),
    path('usuarios/logout/', views.LogoutView, name='logout'),
    path('usuarios/trocarsenha/', views.TrocarSenhaView, name='trocarsenha'),
]

