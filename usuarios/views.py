from django.shortcuts import render, redirect
from django.core.files.storage import FileSystemStorage
from .forms import FormUsuarios
from usuarios.models import Usuarios
from propostas.models import Propostas
from listas import Listas
from utils import Utils
from ligacoes.models import Ligacoes
from datetime import datetime




@Utils.Authenticate
def LoginView(request):
    if request.method == 'POST':
        exist = Usuarios.objects.filter(login=request.POST['email'], senha=request.POST['senha'], status=True)
        if exist.count() > 0:
            request.session['user_key'] = exist[0].id
            return redirect('usuarios/')
        else:
            return redirect('/')

    return render(request, 'usuarios/login.html', {'form': FormUsuarios()})




@Utils.Authenticate
def LogoutView(request):
    del request.session['user_key']
    return redirect('/')




@Utils.Authenticate
def TrocarSenhaView(request):
    usuario = Usuarios.objects.get(id=int(request.session['user_key']))
    if request.method == 'POST':
        if request.POST['senha'] == usuario.senha:
            usuario.senha = request.POST['novasenha']
            usuario.save()
            redirect('/')
    data = {
        'usuario': usuario,
        'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count()
    }
    return render(request, 'usuarios/trocarsenha.html', data)




@Utils.Authenticate
def UsuariosView(request):
    usuario = Usuarios.objects.get(id=int(request.session['user_key']))
    operation = request.GET.get('operation', 'add')
    edicao = request.GET.get('id', None)

    if edicao is not None:
        edicao = Usuarios.objects.get(id=edicao)

    if request.method == 'POST':

        if edicao is not None:
            edicao.nome = request.POST['nome']
            edicao.login = request.POST['login']
            edicao.email = request.POST['email']
            edicao.telefone = request.POST['telefone']
            edicao.banco = request.POST['banco']
            edicao.agencia = request.POST['agencia']
            edicao.conta = request.POST['conta']
            edicao.tipo = request.POST['tipo']
            edicao.nivel = request.POST['nivel']
            cadastro = edicao

            if request.FILES.get('arquivoRG', None) is not None:
                cadastro.arquivoRG =  UploadArquivo(request.FILES['arquivoRG'], 'RG', edicao)
            if request.FILES.get('arquivoCPF', None) is not None:
                cadastro.arquivoCPF = UploadArquivo(request.FILES['arquivoCPF'], 'CPF', edicao)
            if request.FILES.get('arquivoFoto', None) is not None:
                cadastro.arquivoFoto = UploadArquivo(request.FILES['arquivoFoto'], 'Foto', edicao)
            if request.FILES.get('arquivoFicha', None) is not None:
                cadastro.arquivoFicha = UploadArquivo(request.FILES['arquivoFicha'], 'Ficha', edicao)

            cadastro.save()
            return redirect('../usuarios/?operation=list')
        else:
            cadastro = Usuarios(nome=request.POST['nome'],
                                login=request.POST['login'],
                                senha=request.POST['senha'],
                                email=request.POST['email'],
                                telefone=request.POST['telefone'],
                                banco=request.POST['banco'],
                                agencia=request.POST['agencia'],
                                conta=request.POST['conta'],
                                tipo=request.POST['tipo'],
                                nivel=request.POST['nivel'])
            cadastro.save()

            if request.FILES.get('arquivoRG', None) is not None:
                cadastro.arquivoRG =  UploadArquivo(request.FILES['arquivoRG'], 'RG', edicao)
            if request.FILES.get('arquivoCPF', None) is not None:
                cadastro.arquivoCPF = UploadArquivo(request.FILES['arquivoCPF'], 'CPF', edicao)
            if request.FILES.get('arquivoFoto', None) is not None:
                cadastro.arquivoFoto = UploadArquivo(request.FILES['arquivoFoto'], 'Foto', edicao)
            if request.FILES.get('arquivoFicha', None) is not None:
                cadastro.arquivoFicha = UploadArquivo(request.FILES['arquivoFicha'], 'Ficha', edicao)

            cadastro.save()

    data = {
                'agendamentos': Ligacoes.objects.filter(data_agendamento__day=datetime.now().day, data_agendamento__month=datetime.now().month, data_agendamento__year=datetime.now().year).count(),
                'operation': operation, 
                'bancos': Listas.bancos,
                'contas': Listas.tiposDeConta,
                'niveis': Listas.niveisUsuario,
                'usuario': usuario,
                'usuarios': Usuarios.objects.all(),
                'edicao': edicao,
                'operacao': operation,
                'listaStatus': Listas.statusProposta,
                'propostas': Propostas.objects.filter(corretor=edicao, cliente=False) if edicao is not None else None,
                'clientes': Propostas.objects.values('nome', 'cpf').filter(corretor=edicao, cliente=True).distinct().order_by('nome') if edicao is not None else None
            }

    return render(request, 'usuarios/adicionar.html', data)




@Utils.Authenticate
def UploadArquivo(arquivo, nome, usuario):
    
    path = ''
    if arquivo.name is not None:
        myfile = arquivo
        path = 'documentos/' + str(usuario.id) + '/corretor/' + nome + '.' + myfile.name.split('.')[1]
        fs = FileSystemStorage()
        if fs.exists(path):
            fs.delete(path)
        filename = fs.save(path, myfile)

    return path