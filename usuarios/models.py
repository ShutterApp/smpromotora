from django.db import models
from multiselectfield import MultiSelectField
from listas import Listas

class Usuarios(models.Model):

    nome = models.CharField(max_length=254)
    login = models.CharField(max_length=50)
    senha = models.CharField(max_length=10)
    email = models.EmailField(max_length=254)
    telefone = models.CharField(max_length=254)
    agencia = models.CharField(max_length=20)
    conta = models.CharField(max_length=20)
    banco = MultiSelectField(choices=list(Listas.bancos))
    tipo = MultiSelectField(choices=list(Listas.tiposDeConta))
    arquivoRG = models.FileField(blank=True, null=True)
    arquivoCPF = models.FileField(blank=True, null=True)
    arquivoFoto = models.FileField(blank=True, null=True)
    arquivoFicha = models.FileField(blank=True, null=True)
    nivel = MultiSelectField(choices=list(Listas.niveisUsuario))
    pontuacao = models.IntegerField(default=0, blank=True, null=True)
    status = models.BooleanField(default=True, blank=True, null=True)

    def __str__(self):
        return self.nome