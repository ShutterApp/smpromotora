# Lista de bancos
bancos = (
            [0, 'Banco do Brasil '],
            [1, 'Banrisul'],
            [2, 'BGN'],
            [3, 'BMG'],
            [4, 'Bonsucesso'],
            [5, 'Bradesco'],
            [6, 'BV'],
            [7, 'Caixa'],
            [8, 'CCB'],
            [9, 'Cetelen'],
            [10, 'Crefisa'],
            [11, 'Daycoval'],
            [12, 'Help'],
            [13, 'Industrial'],
            [14, 'Intermedium'],
            [15, 'Itaú'],
            [16, 'Itaú BMG'],
            [17, 'Mercantil'],
            [18, 'Olé'],
            [19, 'PAN'],
            [20, 'Paraná'],
            [21, 'Sabemi'],
            [22, 'Safra'],
            [23, 'Santander'],
        )


# Tipos de operação por conta
tiposDeConta = (
                    [0, 'Corrente Pessoa Física'],
                    [1, 'Poupança Pessoa Física'],
                    [2, 'Corrente Pessoa Jurídica'],
                    [3, 'Poupança Pessoa Jurídica'],
                    [4, 'Ordem de pagamento'],
            )


# Níveis de acesso
niveisUsuario = (
    [0, 'Corretor'],
    [1, 'Administrador']
)


# Níveis de acesso
listaProdutos = (
    [0, 'Refinanciamento'],
    [1, 'Contrato novo'],
    [2, 'Débito em conta'],
    [3, 'Portabilidade'],
    [4, 'Refin. + Margem'],
    [5, 'Recompra'],
    [6, 'Cartão'],
)


# Status de propostas
statusProposta = (
    [0, 'Aprovado'],
    [1, 'Reprovado'],
    [2, 'Em Andamento'],
    [3, 'Aguardando Averbação'],
)


# Lista de orgãos
listaOrgaos = (
    [0, 'INSS'],
    [1, 'SIAPE'],
    [2, 'Estado'],
    [3, 'Forças Armadas'],
)

camposProposta = (
    [0, 'contrato'],
    [1, 'banco'],
    [2, 'produto'],
    [3, 'nome'],
    [4, 'sexo'],
    [5, 'cpf'],
    [6, 'rg'],
    [7, 'dataEmissao'],
    [8, 'dataNascimento'],
    [9, 'naturalidade'],
    [10, 'uf'],
    [11, 'nomePai'],
    [12, 'nomeMae'],
    [13, 'endereco'],
    [14, 'numero'],
    [15, 'bairro'],
    [16, 'cidade'],
    [17, 'estado'],
    [18, 'cep'],
    [19, 'orgao'],
    [20, 'funcao'],
    [21, 'matricula'],
    [22, 'senha'],
    [23, 'valorParcela'],
    [24, 'prazo'],
    [25, 'valorBruto'],
    [26, 'valorLiquido'],
    [27, 'renda'],
    [28, 'dataAdmissao'],
    [29, 'numeroCelular'],
    [30, 'numeroFixo'],
    [31, 'correspondente'],
    [32, 'bancoDeposito'],
    [33, 'agenciaPagamento'],
    [34, 'contaPagamento'],
    [35, 'tipoPagamento'],
    [36, 'observacao'],
    [37, 'taxaJuros'],
)


#   Meses

meses = [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul',
    'Ago',
    'Set',
    'Out',
    'Nov',
    'Dez'
]